// linux_dev_user.h



#ifndef LINUX_DEVS_LINUX_DEV_MANAGER_H
#define LINUX_DEVS_LINUX_DEV_MANAGER_H



#include <vector>
#include <ros/ros.h>
#include <linux_devs/DevicesList.h>



namespace linux_devs {



class LinuxDevUser
{
    ros::NodeHandle nodeHandle;

    ros::Subscriber msgDevicesListSub;
    ros::Subscriber msgSelectedDevicesListSub;

    ros::Publisher msgSelectDevicesPub;

    ros::ServiceClient srvGetDeviceInfoCli;

    void TreatDevicesList(const linux_devs::DevicesList::ConstPtr & msg);
    void TreatSelectedDevicesList(const linux_devs::DevicesList::ConstPtr & msg);

protected:
    unsigned char maxDevices;

    virtual void ConfigLinuxDev(ros::NodeHandle & nodeHandle, std::string name = "");

    virtual void DevicesList(const std::vector<std::string> & list) = 0;
    virtual void SelectedDevicesList(const std::vector<std::string> & list) = 0;

    void SelectDevices(std::vector<std::string> & list);

    bool GetDeviceInfo(std::string device, std::map<std::string, std::string> & info);

public:
    LinuxDevUser();
    virtual ~LinuxDevUser();
};



}



#endif
