// linux_dev_manager.h



#ifndef LINUX_DEVS_LINUX_DEV_MANAGER_H
#define LINUX_DEVS_LINUX_DEV_MANAGER_H



#include <vector>
#include <libudev.h>
#include <boost/thread.hpp>
#include <ros/ros.h>
#include <linux_devs/DevicesList.h>
#include <linux_devs/Info.h>



namespace linux_devs {



class DeviceInfo
{
public:
    bool unknown = false;
    std::string sysPath;
    std::string nodePath;
    std::string productID;
    std::string vendorID;
    std::string serialNumber;
    
    std::map<std::string, std::string> attrsMap;

    friend bool operator==(const DeviceInfo & dev1, const DeviceInfo & dev2)
    {
        return (dev1.productID == dev2.productID) && (dev1.vendorID == dev2.vendorID) && (dev1.serialNumber == dev2.serialNumber)
                || (dev1.nodePath == dev2.nodePath);
    }
};



class LinuxDevManager
{
    /*!
     * \brief FillDeviceAttrsMap uses udev to get the requested attrsList 
     *      information about the device.
     *
     * This method will try to locate the information requested by looking at
     * every parent of the device described by sysPath, and returning the first
     * occurrence of the attrs.
     *
     * \param device The device to fill the attrsMap.
     * \param attrsList The list of attrs to look for.
     * \return The number of attrs found from the attrsList.
     */
    int FillDeviceAttrsMap(DeviceInfo* device);

    std::vector<DeviceInfo*> devicesList;
    std::vector<DeviceInfo*> selectedDevices;
    
    std::vector<std::string> attrsSearchList;

    boost::mutex mut; // WHERE ELSE SHOULD THE MUTEX BE USED?

    struct udev* udev;
    struct udev_enumerate* enumerate;
    struct udev_list_entry* devices;
    struct udev_list_entry* devListEntry;
    struct udev_monitor* mon;
    int fileDescriptor;

    bool running;
    boost::thread* thread;

    ros::NodeHandle nodeHandle;

    ros::Subscriber msgSelectDevicesSub;

    ros::Publisher msgDevicesListPub;
    ros::Publisher msgSelectedDevicesListPub;

    ros::ServiceServer srvGetDeviceInfoSer;

    void LoadSettings();
    void SaveSettings();

    std::string GetNextUnknownName();

    void Loop();
    static void Thread(LinuxDevManager* object);

    /*!
     * \brief This function is called before any inclusion is made to LinuxDevManager::devicesList
     *      to filter possible unwanted devices.
     * \param nodePath The nodepath to filter.
     * \return Return true if the device should not be included in the device
     *         list, false otherwise.
     */
    virtual bool FilterDeviceByPath(std::string& nodePath) const
    {
        return Filter::ACCEPT;
    }

    void TreatSelectDevices(const linux_devs::DevicesList::ConstPtr & msg);

    bool TreatGetDeviceInfo(linux_devs::Info::Request & req, linux_devs::Info::Response & res);

protected:
    enum Filter {
        DENY = false, // http://hydra-media.cursecdn.com/dota2.gamepedia.com/8/89/Ss_deny_01.mp3
        ACCEPT = true
    };
    
    void AddAttributeToSearch(const char* attr);

    const DeviceInfo* GetDeviceByNodePath(std::string nodePath);

    std::string netLink;
    std::string subSystem;
    std::string configPath;
    std::string fieldName;

    //void DevicesSelected(std::vector<std::string> list);
    virtual void SelectDevices(const std::vector<std::string> & list) = 0;

public:
    LinuxDevManager();
    virtual void ConfigLinuxDev(ros::NodeHandle & nodeHandle, const std::vector<std::string> & initialSelectedDevices, std::string name = "");
    virtual ~LinuxDevManager();
};



}



#endif
