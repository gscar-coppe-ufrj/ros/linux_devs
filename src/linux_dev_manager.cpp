// linux_dev_manager.cpp



#include <simple_xml_settings/simple_xml_settings.h>
#include <linux_devs/linux_dev_manager.h>



using namespace linux_devs;


linux_devs::LinuxDevManager::LinuxDevManager()
{
    running = true;
    thread = NULL;
    
    AddAttributeToSearch("idProduct");
    AddAttributeToSearch("idVendor");
    AddAttributeToSearch("serial");
}


void linux_devs::LinuxDevManager::LoadSettings()
{
    // Don't load if no config path is specified.
    if (configPath == "")
        return;

    SimpleXMLSettings settings(configPath, fieldName);
    if (settings.HasLock())
    {
        settings.BeginLoad();
        int index = 0;
        while (settings.BeginGroup("Device", index))
        {
            DeviceInfo* info = new DeviceInfo;
            info->unknown = true;
            info->productID = settings.LoadText("productID", "");
            info->vendorID = settings.LoadText("vendorID", "");
            info->serialNumber = settings.LoadText("serialNumber", "");
            info->sysPath = "";
            info->nodePath = GetNextUnknownName();
            info->attrsMap["productID"] = info->productID;
            info->attrsMap["vendorID"] = info->vendorID;
            info->attrsMap["name"] = settings.LoadText("name","");
            info->attrsMap["serial"] = settings.LoadText("serial","");
            devicesList.push_back(info);
            selectedDevices.push_back(info);
            settings.EndGroup();
            index++;
        }
        settings.EndLoad();
    }
}


void linux_devs::LinuxDevManager::SaveSettings()
{
    // Don't save if no config path is specified.
    if (configPath == "")
        return;

    SimpleXMLSettings settings(configPath, fieldName);
    settings.BeginSave();
    int index = 0;
    for (auto info = selectedDevices.begin(); info != selectedDevices.end(); info++)
    {
        settings.BeginGroup("Device", index);
        settings.SaveText("productID", (*info)->productID );
        settings.SaveText("vendorID", (*info)->vendorID);
        settings.SaveText("serialNumber", (*info)->serialNumber);
        settings.SaveText("name", (*info)->attrsMap["name"]);
        settings.SaveText("serial",(*info)->attrsMap["serial"]);
        settings.EndGroup();
        index++;
    }
    settings.EndSave();
}


std::string linux_devs::LinuxDevManager::GetNextUnknownName()
{
    int num = 0;
    while (true)
    {
        auto it = devicesList.begin();
        for (; it != devicesList.end(); it++)
        {
            std::stringstream ss;
            ss << "unknown/" << num;
            if ((*it)->nodePath == ss.str())
            {
                num++;
                break;
            }
        }
        if (it == devicesList.end())
            break;
    }

    std::stringstream ss;
    ss << "unknown/" << num;
    return ss.str();
}


void linux_devs::LinuxDevManager::Loop()
{
    /* Create the udev object */
    udev = udev_new();
    if (!udev)
    {
        std::cout << "Can't create udev" << std::endl;
        return;
    }

    /* This section sets up a monitor which will report events when
       devices attached to the system change.  Events include "add",
       "remove", "change", "online", and "offline".

       This section sets up and starts the monitoring. Events are
       polled for (and delivered) later in the file.

       It is important that the monitor be set up before the call to
       udev_enumerate_scan_devices() so that events (and devices) are
       not missed.  For example, if enumeration happened first, there
       would be no event generated for a device which was attached after
       enumeration but before monitoring began.

       Note that a filter is added so that we only get events for
       "hidraw" devices. */

    /* Set up a monitor to monitor hidraw devices */
    mon = udev_monitor_new_from_netlink(udev, netLink.c_str());
    udev_monitor_filter_add_match_subsystem_devtype(mon, subSystem.c_str(), NULL);
    udev_monitor_enable_receiving(mon);
    /* Get the file descriptor (fd) for the monitor.
       This fd will get passed to select() */
    fileDescriptor = udev_monitor_get_fd(mon);

    /* Create a list of the devices in the 'hidraw' subsystem. */
    enumerate = udev_enumerate_new(udev);
    udev_enumerate_add_match_subsystem(enumerate, subSystem.c_str());
    //udev_enumerate_add_match_property(enumerate, "ID_INPUT_JOYSTICK", "1");
    udev_enumerate_scan_devices(enumerate);
    devices = udev_enumerate_get_list_entry(enumerate);

    /* For each item enumerated, print out its information.
       udev_list_entry_foreach is a macro which expands to
       a loop. The loop will be executed for each member in
       devices, setting dev_list_entry to a list entry
       which contains the device's path in /sys. */
    udev_list_entry_foreach(devListEntry, devices)
    {
        const char *path;

        /* Get the filename of the /sys entry for the device
               and create a udev_device object (dev) representing it */
        path = udev_list_entry_get_name(devListEntry);
        udev_device* dev;
        dev = udev_device_new_from_syspath(udev, path);

        /* usb_device_get_devnode() returns the path to the device node
               itself in /dev. */
        //printf("Device Node Path: %s\n", udev_device_get_devnode(dev));
        bool found = false;
        DeviceInfo* devInfo = new DeviceInfo;
        devInfo->sysPath = path;
        const char* nodePath = udev_device_get_devnode(dev);
        if (nodePath != NULL)
            devInfo->nodePath = nodePath;

        auto devParent = udev_device_get_parent_with_subsystem_devtype(dev, "usb", "usb_device");
        if (devParent != NULL && nodePath != NULL)
        {
            devInfo->productID = udev_device_get_sysattr_value(devParent, "idProduct");
            devInfo->vendorID = udev_device_get_sysattr_value(devParent, "idVendor");
            
            const char* serialNumber = udev_device_get_sysattr_value(devParent, "serial");
            if (serialNumber != NULL)
                devInfo->serialNumber = serialNumber;
                
            FillDeviceAttrsMap(devInfo);

            // Check if the user has already selected the device found and
            // set the device information.
            for (auto it = selectedDevices.begin(); it != selectedDevices.end(); it++)
            {
                if (**it == *devInfo)
                {
                    (*it)->unknown = false;
                    (*it)->sysPath = udev_device_get_syspath(dev);
                    (*it)->nodePath = udev_device_get_devnode(dev);
                    (*it)->productID = devInfo->productID;
                    (*it)->vendorID = devInfo->vendorID;
                    (*it)->serialNumber = devInfo->serialNumber;
                    (*it)->attrsMap = devInfo->attrsMap;
                    found = true;
                }
            }

            if (found)
                delete devInfo;
            else
            {
                if (FilterDeviceByPath(devInfo->nodePath) == Filter::ACCEPT)
                {
                    mut.lock();
                    devicesList.push_back(devInfo);
                    mut.unlock();
                }
            }
        }

        /* The device pointed to by dev contains information about
               the hidraw device. In order to get information about the
               USB device, get the parent device with the
               subsystem/devtype pair of "usb"/"usb_device". This will
               be several levels up the tree, but the function will find
               it.*/
        /*dev = udev_device_get_parent_with_subsystem_devtype(
                    dev,
                    "usb",
                    "usb_device");
        if (!dev) {
            printf("Unable to find parent usb device.");
            exit(1);
        }*/

        /* From here, we can call get_sysattr_value() for each file
               in the device's /sys entry. The strings passed into these
               functions (idProduct, idVendor, serial, etc.) correspond
               directly to the files in the /sys directory which
               represents the USB device. Note that USB strings are
               Unicode, UCS2 encoded, but the strings returned from
               udev_device_get_sysattr_value() are UTF-8 encoded. */
        /*printf("  VID/PID: %s %s\n",
               udev_device_get_sysattr_value(dev,"idVendor"),
               udev_device_get_sysattr_value(dev, "idProduct"));
        printf("  %s\n  %s\n",
               udev_device_get_sysattr_value(dev,"manufacturer"),
               udev_device_get_sysattr_value(dev,"product"));
        printf("  serial: %s\n",
               udev_device_get_sysattr_value(dev, "serial"));*/

        udev_device_unref(dev);
    }
    /* Free the enumerator object */
    udev_enumerate_unref(enumerate);

    // clean up devices that weren't configured
    for (auto i = 0; i != devicesList.size();)
    {
        if (devicesList[i]->sysPath == "" && !devicesList[i]->unknown)
        {
            delete devicesList[i];
            auto found = find(selectedDevices.begin(), selectedDevices.end(), devicesList[i]);
            if (found != selectedDevices.end())
                selectedDevices.erase(found);
            devicesList.erase(i + devicesList.begin());
            continue;
        }
        i++;
    }

    std::vector<std::string> newSelectedDeviceList;
    for (auto it = selectedDevices.begin(); it != selectedDevices.end(); it++)
        newSelectedDeviceList.push_back((*it)->nodePath);
    SelectDevices(newSelectedDeviceList);

    linux_devs::DevicesList msg;
    mut.lock();
    for (auto it = devicesList.begin(); it != devicesList.end(); it++)
    {
        if ((*it)->unknown)
            msg.unknown_devices.push_back((*it)->nodePath);
        else
            msg.devices.push_back((*it)->nodePath);
    }
    mut.unlock();
    msgDevicesListPub.publish(msg);

    DevicesList msg2;
    for (auto it = selectedDevices.begin(); it != selectedDevices.end(); it++)
    {
        if ((*it)->unknown)
            msg2.unknown_devices.push_back((*it)->nodePath);
        else
            msg2.devices.push_back((*it)->nodePath);
    }
    msgSelectedDevicesListPub.publish(msg2);

    // Start the loop
    while (running)
    {
        fd_set fileDescriptiors;
        struct timeval tv;
        int ret = 0;

        FD_ZERO(&fileDescriptiors);
        FD_SET(fileDescriptor, &fileDescriptiors);
        tv.tv_sec = 0;
        tv.tv_usec = 0;

        bool reSend = false;

        mut.lock();
        while (ret = select(fileDescriptor+1, &fileDescriptiors, NULL, NULL, &tv) > 0)
        {

            /* Check if our file descriptor has received data. */
            if (ret > 0 && FD_ISSET(fileDescriptor, &fileDescriptiors))
            {
                //printf("\nselect() says there should be data\n");

                /* Make the call to receive the device.
                   select() ensured that this will not block. */
                udev_device* dev = udev_monitor_receive_device(mon);

                if (dev)
                {
                    std::string path = "";
                    const char* pathNode = udev_device_get_devnode(dev);
                    if (pathNode != NULL)
                        path = pathNode;
                    else
                        continue;

                    std::string action = udev_device_get_action(dev);

                    bool added = false;
                    DeviceInfo devInfo;
                    if (action == "add")
                    {
                        auto devParent = udev_device_get_parent_with_subsystem_devtype(dev, "usb", "usb_device");
                        devInfo.productID = udev_device_get_sysattr_value(devParent, "idProduct");
                        devInfo.vendorID = udev_device_get_sysattr_value(devParent, "idVendor");
                        const char* serialNumber = udev_device_get_sysattr_value(devParent, "serial");
                        if (serialNumber != NULL)
                            devInfo.serialNumber = serialNumber;
                    }

                    for (auto it = devicesList.begin(); it != devicesList.end(); it++)
                    {
                        if (action == "remove" && (*it)->nodePath == path)
                        {
                            DeviceInfo* temp = *it;
                            auto found = find(selectedDevices.begin(), selectedDevices.end(), temp);
                            if (found == selectedDevices.end())
                            {
                                delete temp;
                                devicesList.erase(it);
                            }
                            else
                            {
                                temp->unknown = true;
                                temp->nodePath = GetNextUnknownName();
                                temp->sysPath = "";
                            }
                            reSend = true;

                            break;
                        }

                        if (action == "add" && (*it)->unknown)
                        {
                            if (**it == devInfo && FilterDeviceByPath(path) == Filter::ACCEPT)
                            {
                                (*it)->unknown = false;
                                (*it)->sysPath = udev_device_get_syspath(dev);
                                (*it)->nodePath = path;
                                added = true;
                                reSend = true;
                                break;
                            }
                        }
                    }

                    if (action == "add" && !added)
                    {
                        DeviceInfo* info = new DeviceInfo;
                        info->unknown = false;
                        info->sysPath = udev_device_get_syspath(dev);
                        info->nodePath = path;
                        
                        FillDeviceAttrsMap(info);
                        //devicesList.push_back(info);
                        if (FilterDeviceByPath(info->nodePath) == Filter::ACCEPT)
                        {
                            devicesList.push_back(info);
                        }

                        reSend = true;
                    }

                    /*printf("Got Device\n");
                    printf("   Node: %s\n", udev_device_get_devnode(dev));
                    printf("   Subsystem: %s\n", udev_device_get_subsystem(dev));
                    printf("   Devtype: %s\n", udev_device_get_devtype(dev));

                    printf("   Action: %s\n", udev_device_get_action(dev));*/

                    udev_device_unref(dev);
                }
                /*else {
                    printf("No Device from receive_device(). An error occured.\n");
                }*/
            }
        }

        if (reSend)
        {
            std::vector<std::string> newSelectedDeviceList;
            for (auto it = selectedDevices.begin(); it != selectedDevices.end(); it++)
                newSelectedDeviceList.push_back((*it)->nodePath);
            SelectDevices(newSelectedDeviceList);

            DevicesList msg;
            for (auto it = devicesList.begin(); it != devicesList.end(); it++)
            {
                if ((*it)->unknown)
                    msg.unknown_devices.push_back((*it)->nodePath);
                else
                    msg.devices.push_back((*it)->nodePath);
            }
            msgDevicesListPub.publish(msg);

            DevicesList msg2;
            for (auto it = selectedDevices.begin(); it != selectedDevices.end(); it++)
            {
                if ((*it)->unknown)
                    msg2.unknown_devices.push_back((*it)->nodePath);
                else
                    msg2.devices.push_back((*it)->nodePath);
            }
            msgSelectedDevicesListPub.publish(msg2);
        }
        mut.unlock();

        boost::this_thread::sleep(boost::posix_time::seconds(5));
    }
}


void linux_devs::LinuxDevManager::Thread(linux_devs::LinuxDevManager* object)
{
    object->Loop();
}


void linux_devs::LinuxDevManager::TreatSelectDevices(const linux_devs::DevicesList::ConstPtr & msg)
{
    mut.lock();

    std::vector<std::string> newSelectedDeviceList;
    std::vector<std::string> devicesNotSelectedAnymoreList;

    for (auto dev = selectedDevices.begin(); dev != selectedDevices.end(); dev++)
    {
        auto found = find(msg->devices.begin(), msg->devices.end(), (*dev)->nodePath);
        if (found == msg->devices.end())
            devicesNotSelectedAnymoreList.push_back((*dev)->nodePath);
    }

    selectedDevices.clear();
    for (auto dev = msg->devices.begin(); dev != msg->devices.end(); dev++)
    {
        for (auto devRegistered = devicesList.begin(); devRegistered != devicesList.end(); devRegistered++)
        {
            if ((*devRegistered)->nodePath == (*dev))
            {
                selectedDevices.push_back(*devRegistered);
                newSelectedDeviceList.push_back(*dev);
                break;
            }
        }
    }

    for (auto i = 0; i < devicesList.size();)
    {
        auto found = find(devicesNotSelectedAnymoreList.begin(), devicesNotSelectedAnymoreList.end(), devicesList[i]->nodePath);
        if (found != devicesNotSelectedAnymoreList.end())
        {
            if (devicesList[i]->unknown)
            {
                delete devicesList[i];
                devicesList.erase(devicesList.begin() + i);
                continue;
            }
        }
        i++;
    }


    DevicesList msg2;
    for (auto it = devicesList.begin(); it != devicesList.end(); it++)
    {
        if ((*it)->unknown)
            msg2.unknown_devices.push_back((*it)->nodePath);
        else
            msg2.devices.push_back((*it)->nodePath);
    }
    msgDevicesListPub.publish(msg2);

    DevicesList msg3;
    for (auto it = selectedDevices.begin(); it != selectedDevices.end(); it++)
    {
        if ((*it)->unknown)
            msg3.unknown_devices.push_back((*it)->nodePath);
        else
            msg3.devices.push_back((*it)->nodePath);
    }
    msgSelectedDevicesListPub.publish(msg3);

    mut.unlock();

    SelectDevices(newSelectedDeviceList);

}


void linux_devs::LinuxDevManager::AddAttributeToSearch(const char* attr)
{
    attrsSearchList.push_back(attr);
}


int linux_devs::LinuxDevManager::FillDeviceAttrsMap(DeviceInfo* device)
{
    int found = 0;
    std::string sysPath = device->sysPath;
    
    // This function is called inside the main loop, so udev must have been
    // initialized. If this fails probably (1 == 1) fails too ;)
    if (udev == NULL)
        return -1;
    
    udev_device* dev;
    dev = udev_device_new_from_syspath(udev, sysPath.c_str());
    
    if (dev == NULL)
        return -1;
        
    for (int i = 0; i < attrsSearchList.size(); i++)
    {
        std::string currentSearch = attrsSearchList[i];
        udev_device* devParent = dev;
    
        while (devParent != NULL)
        {
            const char* attr = udev_device_get_sysattr_value(devParent, currentSearch.c_str());
            
            if (attr != NULL)
            {
                device->attrsMap[currentSearch] = std::string(attr);
                found++;
                break;
            }
            
            devParent = udev_device_get_parent(devParent);
        }
    }    
    
    return found;
}


const DeviceInfo* LinuxDevManager::GetDeviceByNodePath(std::string nodePath)
{
    for (auto it = devicesList.begin(); it != devicesList.end(); it++)
    {
        if ((*it)->nodePath == nodePath)
            return *it;
    }
    
    return NULL;
}


bool linux_devs::LinuxDevManager::TreatGetDeviceInfo(linux_devs::Info::Request & req, linux_devs::Info::Response & res)
{
    res.success = false;

    mut.lock();
    for (auto it = devicesList.begin(); it != devicesList.end(); it++)
    {
        if ((*it)->nodePath == req.device)
        {
            for (auto attr = (*it)->attrsMap.begin(); attr != (*it)->attrsMap.end(); attr++)
            {
                res.infoName.push_back(attr->first);
                res.infoValue.push_back(attr->second);
            }

            res.success = true;
            break;
        }
    }
    mut.unlock();

    return res.success;
}


/*void linux_devs::LinuxDevManager::DevicesSelected(std::vector<std::string> list)
{
    linux_devs::DevicesList msg;
    msg.devices = list;
    msgSelectedDevicesListPub.publish(msg);
}*/


void linux_devs::LinuxDevManager::ConfigLinuxDev(ros::NodeHandle & nodeHandle, const std::vector<std::string> & initialSelectedDevices, std::string name)
{
    for (auto it = initialSelectedDevices.begin(); it != initialSelectedDevices.end(); it++)
    {
        DeviceInfo* devInfo = new DeviceInfo;
        devInfo->nodePath = *it;
        devicesList.push_back(devInfo);
        selectedDevices.push_back(devInfo);
    }

    this->nodeHandle = nodeHandle;

    if (name != "")
        name += "/";

    msgSelectDevicesSub = nodeHandle.subscribe(name + "select_devices", 100, &LinuxDevManager::TreatSelectDevices, this);

    msgDevicesListPub = nodeHandle.advertise<linux_devs::DevicesList>(name + "devices_list", 100, true);
    msgSelectedDevicesListPub = nodeHandle.advertise<linux_devs::DevicesList>(name + "selected_devices_list", 100, true);

    srvGetDeviceInfoSer = nodeHandle.advertiseService(name + "device_info", &LinuxDevManager::TreatGetDeviceInfo, this);

    LoadSettings();

    thread = new boost::thread(LinuxDevManager::Thread, this);
}


linux_devs::LinuxDevManager::~LinuxDevManager()
{
    SaveSettings();

    running = false;
    if (thread != NULL)
    {
        thread->interrupt();
        thread->join();
    }
}
