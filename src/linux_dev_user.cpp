// linux_dev_user.cpp



#include <linux_devs/Info.h>
#include <linux_devs/linux_dev_user.h>



linux_devs::LinuxDevUser::LinuxDevUser()
{
}


void linux_devs::LinuxDevUser::TreatDevicesList(const linux_devs::DevicesList::ConstPtr & msg)
{
    auto temp = msg->devices;
    temp.insert(temp.end(), msg->unknown_devices.begin(), msg->unknown_devices.end());
    DevicesList(temp);
}


void linux_devs::LinuxDevUser::TreatSelectedDevicesList(const linux_devs::DevicesList::ConstPtr & msg)
{
    auto temp = msg->devices;
    temp.insert(temp.end(), msg->unknown_devices.begin(), msg->unknown_devices.end());
    SelectedDevicesList(temp);
}


void linux_devs::LinuxDevUser::ConfigLinuxDev(ros::NodeHandle & nodeHandle, std::string name)
{
    this->nodeHandle = nodeHandle;

    if (name != "")
        name += "/";

    msgDevicesListSub = nodeHandle.subscribe(name + "devices_list", 100, &LinuxDevUser::TreatDevicesList, this);
    msgSelectedDevicesListSub = nodeHandle.subscribe(name + "selected_devices_list", 100, &LinuxDevUser::TreatSelectedDevicesList, this);

    msgSelectDevicesPub = nodeHandle.advertise<linux_devs::DevicesList>(name + "select_devices", 100, true);

    srvGetDeviceInfoCli = nodeHandle.serviceClient<linux_devs::Info>(name + "device_info");
}


void linux_devs::LinuxDevUser::SelectDevices(std::vector<std::string> & list)
{
    linux_devs::DevicesList msg;
    msg.devices = list;
    msgSelectDevicesPub.publish(msg);
}


bool linux_devs::LinuxDevUser::GetDeviceInfo(std::string device, 
                                std::map<std::string, std::string> & info)
{
    linux_devs::Info srv;
    srv.request.device = device;
    if (srvGetDeviceInfoCli.call(srv))
    {
        if (srv.response.success)
        {
            for (int i = 0; i < srv.response.infoName.size(); i++)
            {
                info[srv.response.infoName[i]] = srv.response.infoValue[i];
            }
            return true;
        }
    }
    return false;
}


linux_devs::LinuxDevUser::~LinuxDevUser()
{
}
